from flask import Flask, render_template, request, redirect
import firebase_admin
from firebase_admin import credentials, db, firestore

app = Flask(__name__)

# Help with Firestore :
# https://faun.pub/getting-started-with-firebase-cloud-firestore-using-python-c6ab3f5ecae0

cred = credentials.Certificate("python-eval-8c679-firebase-adminsdk-lutmo-8cf20f6a5f.json")
firebase_admin.initialize_app(cred)
firestore_db = firestore.client()


@app.route("/save/", methods=["POST"])
def add():
    firestore_db.collection(u"notes").add({
        "date": request.form["date"], 
        "former": request.form["former"],
        "note": request.form["note"],
        "students": request.form.getlist("students")
    })
    return redirect("/enregistrement effectué avec succés !")



@app.route("/")
@app.route("/<message>")
def index(message=""):
    formers = firestore_db.collection(u"formers").stream()
    students = firestore_db.collection(u"students").stream()
    notes = firestore_db.collection(u"notes").stream()

    return render_template("home.html", notes=notes, students=students, formers=formers, message=message)

