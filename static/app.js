
/*
    Afficher / cacher le formulaire d'ajout de note
*/

function showHideAddForm() {
    let addForm = document.getElementById("addForm");
    $(addForm).animate({ height: "toggle" }, 300, "swing");
}

document
    .getElementById("trigger_add")
    .addEventListener("click", showHideAddForm);



/*
    Système de restriction par apprenant
*/

function doRestrictByStudent() {
    student = document.getElementById("trigger_select").value;
    list = document.querySelectorAll("table.table tbody tr");

    if (student == "*") {
        list.forEach(e => e.style.display = "table-row");
    } else {
        list.forEach(e => e.style.display = e.innerHTML.includes(student) ? "table-row" : "none");
    }
}

document
    .getElementById("trigger_select")
    .addEventListener("click", doRestrictByStudent);



/*
    Date par défaut à aujourd'hui
*/

document
    .getElementById("date")
    .valueAsDate = new Date();




/*
    Datatables
*/

$(document).ready(function () {
    $('.table').DataTable({
        paging: false,
        ordering: true,
        searching: true,
        language: {
            url: '/static/dataTables.french.json'
        }
    });
});




/*
    Notification se cache automatiquement
*/

function doCloseAlert() {
    $('.alert').animate({ opacity: "toggle" }, 1500, "linear");
}

setTimeout(doCloseAlert, 2000);