# Evaluation "Journal de bord"

A Python/Flask/Firestore/Bootstrap/jQuery/DataTables program

# install

- python3 -m venv venv
- pip install Flask
- npm install
- configure Firestore, get your JSON file
- configure "cred" variable  (app.py, l.10)

# launch

- ./start.sh